public class GCD {
    //метод нахождения наибольшего общего делителя четырех натуральных чисел
    public static void main(String[] args) {
        int[] nums = {4, 8, 16, 24};
        int gcd = nums[0];
        for (int i = 1; i < nums.length; i++) {
            gcd = gsd(gcd, nums[i]);
        }
        System.out.println("GCD: " + gcd);
    }
    public static int gsd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gsd(b, a % b);
    }
}